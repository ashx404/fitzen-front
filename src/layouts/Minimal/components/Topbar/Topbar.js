import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { AppBar, Toolbar,Typography } from '@material-ui/core';

const useStyles = makeStyles(() => ({
  logo:{
    fontFamily:"Lato",
    textDecoration:"none",
    fontSize:"20px",
    color:"white",
    fontWeight:"bolder"
   },
  root: {
    boxShadow: 'none',
    background: 'linear-gradient(to right bottom, #430089, #82ffa1)'
  }
}));

const Topbar = props => {
  const { className, ...rest } = props;

  const classes = useStyles();

  return (
    <AppBar
      {...rest}
      className={clsx(classes.root, className)}
      color="primary"
      position="fixed"
      style={{paddingTop:0}}
    >
      <Toolbar>
      <RouterLink to="/sign-in">
      <Typography className={classes.logo}>
          <img
            alt="Logo"
            src="/images/logos/fitsen-logo3.png"
            width="30%"
          /> 
          {/* Fitsen */}
          </Typography>
        </RouterLink>
      
      </Toolbar>
    </AppBar>
  );
};

Topbar.propTypes = {
  className: PropTypes.string
};

export default Topbar;

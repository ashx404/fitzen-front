import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/styles';
import { IconButton, Grid, Typography } from '@material-ui/core';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import Skeleton from  '@material-ui/lab/Skeleton';
import  ProductCard  from './components/ProductCard/ProductCard.js';
import { UsersToolbar, UsersTable } from './components';
import mockData from './data';
import axios from 'axios';
const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3),
    paddingBottom:50
  },
  content: {
    marginTop: theme.spacing(6)
  }
}));
var products=[];
const UserList = () => {

    const classes = useStyles();
    const [products1,setproducts] = useState(<React.Fragment>
         <Grid
      container
      spacing={3}
    >
    <Grid
      item
    // style={{display:'block'}}
      lg={4}
      md={6}
      xs={12}
    >
      <Skeleton  animation="wave" variant="rect" width={400} height={170} style={{marginBottom:'30'}} /> 
 <div style={{marginBottom:'30',display:"hidden"}} ><br/> </div>
      <Skeleton  animation="wave" variant="rect" width={400} height={40} />

    </Grid>
    <Grid
    item
  
    lg={4}
    md={6}
    xs={12}
  >
      <Skeleton  animation="wave" variant="rect" width={400} height={170} style={{marginBottom:'30'}} /> 
 <div style={{marginBottom:'30',display:"hidden"}} ><br/> </div>
      <Skeleton  animation="wave" variant="rect" width={400} height={40} />
    </Grid>
    <Grid
    item
  
    lg={4}
    md={6}
    xs={12}
  >
      <Skeleton  animation="wave" variant="rect" width={400} height={170} style={{marginBottom:'30'}} /> 
 <div style={{marginBottom:'30',display:"hidden"}} ><br/> </div>
      <Skeleton  animation="wave" variant="rect" width={400} height={40} />
    </Grid>
    <Grid
    item
  
    lg={4}
    md={6}
    xs={12}
  >
      <Skeleton  animation="wave" variant="rect" width={400} height={170} style={{marginBottom:'30'}} /> 
 <div style={{marginBottom:'30',display:"hidden"}} ><br/> </div>
      <Skeleton  animation="wave" variant="rect" width={400} height={40} />
    </Grid>
    <Grid
    item
  
    lg={4}
    md={6}
    xs={12}
  >
      <Skeleton  animation="wave" variant="rect" width={400} height={170} style={{marginBottom:'30'}} /> 
 <div style={{marginBottom:'30',display:"hidden"}} ><br/> </div>
      <Skeleton  animation="wave" variant="rect" width={400} height={40} />
    </Grid>
    <Grid
    item
  
    lg={4}
    md={6}
    xs={12}
  >
      <Skeleton  animation="wave" variant="rect" width={400} height={170} style={{marginBottom:'30'}} /> 
 <div style={{marginBottom:'30',display:"hidden"}} ><br/> </div>
      <Skeleton  animation="wave" variant="rect" width={400} height={40} />
    </Grid>
  
  </Grid>
  </React.Fragment>);

useEffect(()=>{
  document.title="Fitsen / Trainers";
  axios.get('https://fitsen.herokuapp.com/getTrainers').then(res=>{

  products=Object.values(res.data);
 setproducts(         <Grid
  container
  spacing={3}
>
  {products[0].map(product => (

    <Grid
      item
      key={product._id}
      lg={4}
      md={6}
      xs={12}
    >
<ProductCard product={product} />
    </Grid>
  ))}
  </Grid>)



})})
  return(
  <div className={classes.root}>
  {/* <ProductsToolbar /> */}
  <div className={classes.content}>
 
{products1}

  </div>

</div>


  );
  // return (
  //   <div className={classes.root}>
  //     {/* <UsersToolbar /> */}
  //     <div className={classes.content} style={{marginTop:100}}>
  //       <UsersTable users={users} />
  //     </div>
  //   </div>
  // );
};

export default UserList;

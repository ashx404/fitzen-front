import React, { useState, useEffect } from 'react';
import { Link as RouterLink, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import validate from 'validate.js';
import { makeStyles } from '@material-ui/styles';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import axios from 'axios';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import Typical from "react-typical";
import FormLabel from '@material-ui/core/FormLabel';
import {
  Grid,
  Button,
  IconButton,
  TextField,
  Link,
  FormHelperText,
  Checkbox,
  Typography
} from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

const schema = {
  firstName: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      maximum: 32
    }
  },
  lastName: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      maximum: 32
    }
  },
  email: {
    presence: { allowEmpty: false, message: 'is required' },
    email: true,
    length: {
      maximum: 64
    }
  },
  password: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      maximum: 128
    }
  },
  charges: {
    presence: { allowEmpty: false, message: 'is required' },

  },
  experience: {
    presence: { allowEmpty: false, message: 'is required' },
    
  },
  about: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      maximum: 300
    }
  },
  // file1: {
  //   presence: { allowEmpty: false, message: 'is required' },

  // },
  // file2: {
  //   presence: { allowEmpty: false, message: 'is required' },

  // },
  policy: {
    presence: { allowEmpty: false, message: 'is required' },
    checked: true
  }
};

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.default,
    height: '100%'
  },
  grid: {
    height: '100%'
  },
  quoteContainer: {
    [theme.breakpoints.down('md')]: {
      display: 'none'
    }
  },
  quote: {
    backgroundColor: theme.palette.neutral,
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundImage: 'url(/images/auth.jpg)',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center'
  },
  quoteInner: {
    textAlign: 'center',
    flexBasis: '600px'
  },
  quoteText: {
    color: theme.palette.white,
    fontWeight: 300
  },
  name: {
    marginTop: theme.spacing(3),
    color: theme.palette.white
  },
  bio: {
    color: theme.palette.white
  },
  contentContainer: {},
  content: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column'
  },
  contentHeader: {
    display: 'flex',
    alignItems: 'center',
    paddingTop: theme.spacing(5),
    paddingBototm: theme.spacing(2),
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2)
  },
  logoImage: {
    marginLeft: theme.spacing(4)
  },
  contentBody: {
    flexGrow: 1,
    display: 'flex',
    alignItems: 'center',
    [theme.breakpoints.down('md')]: {
      justifyContent: 'center'
    }
  },
  form: {
    paddingLeft: 100,
    paddingRight: 100,
    paddingBottom: 125,
    flexBasis: 700,
    [theme.breakpoints.down('sm')]: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(2)
    }
  },
  title: {
    marginTop: theme.spacing(3)
  },
  textField: {
    marginTop: theme.spacing(2)
  },
  policy: {
    marginTop: theme.spacing(1),
    display: 'flex',
    alignItems: 'center'
  },
  policyCheckbox: {
    marginLeft: '-14px'
  },
  signUpButton: {
    margin: theme.spacing(2, 0)
  },
  formControl: {
    margin: theme.spacing(3),
  }
}));

const SignUp = props => {
  const { history } = props;
  let f1=false;
  let f2=false;

  const classes = useStyles();
  const [value1, setValue] = React.useState('Trainer');
  const [filevalue1,setfileValue1]=React.useState(null);
  const [filevalue2,setfileValue2]=React.useState(null);
  const [formState, setFormState] = useState({
    isValid: false,
    values: {},
    touched: {},
    errors: {}
  });

  useEffect(() => {
    const errors = validate(formState.values, schema);

    setFormState(formState => ({
      ...formState,
      isValid: errors ? false : true,
      errors: errors || {}
    }));
  }, [formState.values]);

  const handleChange = event => {
    event.persist();

    setFormState(formState => ({
      ...formState,
      values: {
        ...formState.values,
        [event.target.name]:
          event.target.type === 'checkbox'
            ? event.target.checked
            : event.target.value
      },
      touched: {
        ...formState.touched,
        [event.target.name]: true
      }
    }));
  };
  const handleChange1 = event => {
    setValue(event.target.value);
    

  };
  // const handleBack = () => {
  //   history.goBack();
  // };
const handlefile1Change= event =>{
  
  setfileValue1(event.target.files[0]);
  f1=true;console.log("AGAm f1" +f1)
}
const handlefile2Change= event =>{
  setfileValue2(event.target.files[0]);

}
  const handleSignUp = event => {
    event.preventDefault();
 
    let formd ={};
    // console.log(formd)

    // console.log(formState.values);
    // formState.values.append('file1',filevalue1);
    // formState.values.append('file2',filevalue2);
    // formState.values.append('file3',value);
formd=Object.assign({'email':formState.values.email},formd);
formd=Object.assign({'firstName':formState.values.firstName},formd);
formd=Object.assign({'lastName':formState.values.lastName},formd);
formd=Object.assign({'password':formState.values.password},formd);
formd=Object.assign({'charges':formState.values.charges},formd);
formd=Object.assign({'experience':formState.values.experience},formd);
formd=Object.assign({'file1':filevalue1},formd);
formd=Object.assign({'file2':filevalue2},formd);

axios.post('https://fitsen.herokuapp.com/newMentor',{'email':formState.values.email,'firstName':formState.values.firstName,'lastName':formState.values.lastName,'password':formState.values.password,'sessionCharges':formState.values.charges,'experience':formState.values.experience,'about':formState.values.about,'file1':filevalue1,'file2':filevalue2,'role':value1}).then(res=>{

})



    // console.log(formd);

  };

  // const hasError = field =>
  //   formState.touched[field] && formState.errors[field] ? true : false;

  return (
    <div className={classes.root}>
      <Grid
        className={classes.grid}
        container
      >
        <Grid
          className={classes.quoteContainer}
          item
          lg={5}
        >
          <div className={classes.quote}>
            {/* <div className={classes.quoteInner}>
              <Typography
                className={classes.quoteText}
                variant="h1"
              >
                Hella narwhal Cosby sweater McSweeney's, salvia kitsch before
                they sold out High Life.
              </Typography>
              <div className={classes.person}>
                <Typography
                  className={classes.name}
                  variant="body1"
                >
                  Takamaru Ayako
                </Typography>
                <Typography
                  className={classes.bio}
                  variant="body2"
                >
                  Manager at inVision
                </Typography>
              </div>
            </div>
          */}

<Typical
            steps={["Trainer ?", 2000,"Therapist ? ", 1000, "Nutritionist ?", 1000 ,"Register Here !",1000]}
            loop={Infinity}
            wrapper="h1"
            style={{size:"300"}}
            className="typical changingfont"
          />
          </div>
        </Grid>
        <Grid
          className={classes.content}
          item
          lg={7}
          xs={12}
        >
          <div className={classes.content}>
            {/* <div className={classes.contentHeader}>
              <IconButton onClick={handleBack}>
                <ArrowBackIcon />
              </IconButton>
            </div> */}
            <Typography
        className={classes.title}
        variant="h1"
        align='center' 
        lg={4}
        style={{width:"100%",color:"Black", fontFamily:"Lato",fontWeight:'bolder',paddingBottom:"1%"}}
      > <img
      alt="Logo"
      src="/images/logos/fitsen-logo1.png" /></Typography>
            <div className={classes.contentBody}>
              <form
                className={classes.form}
                onSubmit={handleSignUp}
                id="myForm"
              >
                <Typography
                  className={classes.title}
                  variant="h2"
                >
                  Create new account
                </Typography>
                <Typography
                  color="textSecondary"
                  gutterBottom
                >
                  Use your email to create new account
                </Typography>
                <TextField
                  className={classes.textField}
          
                  fullWidth
                 
                  label="First name"
                  name="firstName"
                  onChange={handleChange}
                  type="text"
                 
                  variant="outlined"
                />
                <TextField
                  className={classes.textField}
           
                fullWidth
                  label="Last name"
                  name="lastName"
                  onChange={handleChange}
                  type="text"
                  
                  variant="outlined"
                />
                <TextField
                  className={classes.textField}
                  
                  fullWidth
                  
                  label="Email address"
                  name="email"
                  onChange={handleChange}
                  type="text"

                  variant="outlined"
                />
                <TextField
                  className={classes.textField}
                  
                  fullWidth
                
                  label="Password"
                  name="password"
                  onChange={handleChange}
                  type="password"
                  value={formState.values.password || ''}
                  variant="outlined"
                  
                /><FormControl component="fieldset" className={classes.textField}>
                <FormLabel component="legend">Applying As</FormLabel>
                <RadioGroup aria-label="Position" style={{display:'flex'}} name="mentortype" value={value1} onChange={handleChange1} row>
                  <FormControlLabel value="Trainer" control={<Radio />} label="Trainer" />
                  <FormControlLabel value="Nutritionist" control={<Radio />} label="Nutritionist" />
                  <FormControlLabel value="Therapist" control={<Radio />} label="Therapist" />
               </RadioGroup>
      </FormControl> <TextField
      className={classes.textField}
          id="outlined-number"
          label="Years of Experience"
          placeholder="Years of Experience in relative field"
          type="number"
          name="experience"
          fullWidth
          inputProps={{ min: "0"}}
          onChange={handleChange}
          InputLabelProps={{
            shrink: true,
          }}
          variant="outlined"
        />
        <TextField
      className={classes.textField}
          id="outlined-number"
          label="Session Charges"
          placeholder="Session Charge"
          name="charges"
          type="number"
          fullWidth
          inputProps={{ min: "0",step:"50"}}
          onChange={handleChange}
          InputLabelProps={{
            shrink: true,
          }}
          variant="outlined"
        />
                     <TextField
                     className={classes.textField}
          id="outlined-textarea"
          label="About You"
          name="about"
          placeholder="About You in 20 words"
          multiline
          fullWidth
          onChange={handleChange}
          variant="outlined"
          style={{marginBottom:20}}
        />
                                <Typography
                                t={3}
                  color="textSecondary"
       
                  gutterBottom
                  variant="body1"
                  
                >

                  Upload your certificates
                </Typography>
                <input type="file" onChange={handlefile1Change} name='file1'/>
                <input type="file" onChange={handlefile2Change} name='file2'/>
                <div className={classes.policy}>
                  <Checkbox
                    // checked={formState.values.policy || false}
                    className={classes.policyCheckbox}
                    color="primary"
                    name="policy"
                    onChange={handleChange}
                  />
                  <Typography
                    className={classes.policyText}
                    color="textSecondary"
                    variant="body1"
                  >
                    I have read the{' '}
                    <Link
                      color="primary"
                      component={RouterLink}
                      to="#"
                      underline="always"
                      variant="h6"
                    >
                      Terms and Conditions
                    </Link>
                  </Typography>
                </div>

                <Button
                  className={classes.signUpButton}
                  color="primary"
                  disabled={!(formState.isValid)}
                  fullWidth
                  size="large"
                  type="submit"
                  variant="contained"
                >
                  Sign up now
                </Button>
                <Typography
                  color="textSecondary"
                  variant="body1"
                >
                  Have an account?{' '}
                  <Link
                    component={RouterLink}
                    to="/mentor/signin"
                    variant="h6"
                  >
                    Sign in
                  </Link>
                </Typography>
              </form>
            </div>
          </div>
        </Grid>
      </Grid>
    </div>
  );
};

SignUp.propTypes = {
  history: PropTypes.object
};

export default withRouter(SignUp);

import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import { RouteWithLayout } from './components';
import { Main as MainLayout, Minimal as MinimalLayout } from './layouts';
import  CoachMain from './layouts/MentorMain/Main'; 

import {
  Dashboard as DashboardView,
  ProductList as ProductListView,
  UserList as UserListView,
  Typography as TypographyView,
  Icons as IconsView,
  Account as AccountView,
  Settings as SettingsView,
  SignUp as SignUpView,
  SignIn as SignInView,
  NotFound as NotFoundView,
 
 
} from './views';
import  Nutritionist  from './views/Nutritionist/NutritionistList';
import MentorSignin from './views/mentorSignIn/SignIn';
import Homepage from './views/Homepage/Homepage';
import  Auth  from './views/Auth/Auth';
import CoachDashboard from './views/CoachDashboard/Dashboard';
import ClientList from './views/ClientList/ClientList';
const Routes = () => {
  return (
    <Switch>
      <Redirect
        exact
        from="/"
        to="/home"
      />
      <RouteWithLayout
        component={DashboardView}
        exact
        layout={MainLayout}
        path="/user/dashboard"
      />
      <RouteWithLayout
        component={UserListView}
        exact
        layout={MainLayout}
        path="/user/trainers"
      />
      <RouteWithLayout
        component={ProductListView}
        exact
        layout={MainLayout}
        path="/user/therapists"
      />
            <RouteWithLayout
        component={Nutritionist}
        exact
        layout={MainLayout}
        path="/user/nutritionists"
      />
      <RouteWithLayout
        component={ClientList}
        exact
        layout={CoachMain}
        path="/coach/dashboard"
      />
      {/* <RouteWithLayout
        component={}
        exact
        layout={CoachMain}
        path="/coach/clients"
      /> */}
      <RouteWithLayout
        component={AccountView}
        exact
        layout={MainLayout}
        path="/account"
      />
      {/* <RouteWithLayout
        component={SettingsView}
        exact
        layout={MainLayout}
        path="/settings"
      /> */}
      <Route
        component={SignUpView}
        exact
        // layout={MinimalLayout}
        path="/sign-up"
      />
      <Route
        component={SignInView}
        exact
        // layout={MinimalLayout}
        path="/sign-in"
      />
            <Route
        component={MentorSignin}
        exact
      //  layout={MinimalLayout}
        path="/mentor/signin"
      />
      <RouteWithLayout
        component={NotFoundView}
        exact
        layout={MinimalLayout}
        path="/not-found"
      />
            <Route
        component={Auth}
        exact
        
        path="/auth"
      />
                  <Route
        component={Homepage}
        exact
        
        path="/home"
      />
      <Redirect to="/not-found" />
    </Switch>
  );
};

export default Routes;
